#ifndef RELAISCTL_h
#define RELAISCTL_h
#include <Arduino.h>

class RelaisCtl {
 public:
  RelaisCtl(int pin, int initState = LOW);
  void init();
  void on();
  void off();
  void toggle();
  void reset();
  int getState();

 private:
  int _relaisPin;
  int _state;
  int _initState;
};

#endif