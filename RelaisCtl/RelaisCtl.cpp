#include "RelaisCtl.h"

RelaisCtl::RelaisCtl(int pin, int initState) {
  _relaisPin = pin;
  _state = initState;
  _initState = initState;
}

void RelaisCtl::init() {
  pinMode(_relaisPin, OUTPUT);
  digitalWrite(_relaisPin, _initState);
  _state = _initState;
}

void RelaisCtl::on() {
  if (_state != HIGH) {
    digitalWrite(_relaisPin, HIGH);
    _state = HIGH;
  }
}

void RelaisCtl::off() {
  if (_state != LOW) {
    digitalWrite(_relaisPin, LOW);
    _state = LOW;
  }
}

void RelaisCtl::toggle() {
  digitalWrite(_relaisPin, !_state);
  _state = !_state;
}

void RelaisCtl::reset() {
  digitalWrite(_relaisPin, _initState);
  _state = _initState;
}

int RelaisCtl::getState() { return _state; }