#ifndef WATERLEVELCTLCONSTANTS_H
#define WATERLEVELCTLCONSTANTS_H

#define WL_MEASURE_INTERVAL 300  // in milliseconds

// max defines how many readings will be taken
// offset defines how many of them will be rejected in the avarage calculation
// the sonsor is quiet oscilating, so we wanna take a ton of measures
// and the offset removes after sorting the lowest and the highest values
// therefore max - (2 * offset) should not be less then 1
#define WL_MEASURE_MAX 60
#define WL_FLATEN_OFFSET 29

#endif