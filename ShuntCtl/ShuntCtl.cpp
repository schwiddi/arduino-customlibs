#include "ShuntCtl.h"

ShuntCtl::ShuntCtl(INA219_I2C_ADDRESS address, ErrorHandler errorHandler)
    : _address(address), _shunt(&Wire, address), _errorHandler(errorHandler) {}

void ShuntCtl::init() {
  _state = INIT;
  // init digital wattmeter
  int trys = 0;
  const int maxTries = 10;

  while (!_shunt.begin() && trys < maxTries) {
    trys++;
    delay(200);
  }

  if (trys == maxTries) {
    Serial.print("ShuntCtl.init - connection to sensor failed on address: ");
    Serial.println(_address);
    _state = FAILURE;
    _errorHandler();
  } else {
    Serial.print("ShuntCtl.init - sensor successfully initialized: ");
    Serial.println(_address);
  }
}

void ShuntCtl::measure() {
  switch (_state) {
    case INIT:
      _lastRuntime = millis();
      _state = MEASURE;
      break;
    case MEASURE:
      _currentRuntime = millis();

      if (_currentRuntime - _lastRuntime > _interval) {
        _busVoltage_V = _shunt.getBusVoltage_V();
        _shuntVoltage_mV = _shunt.getShuntVoltage_mV();
        _current_mA = _shunt.getCurrent_mA();
        _power_mW = _shunt.getPower_mW();
        Serial.print("ShuntCtl.measure - measuring busVoltage_V: ");
        Serial.print(_busVoltage_V);
        Serial.print(" shuntVoltage_mV: ");
        Serial.print(_shuntVoltage_mV);
        Serial.print(" current_mA: ");
        Serial.print(_current_mA);
        Serial.print(" power_mW: ");
        Serial.print(_power_mW);
        Serial.println();

        _lastRuntime = _currentRuntime;

        Serial.print("ShuntCtl.measure - last operation status: ");
        Serial.println(_shunt.lastOperateStatus);
        if (_shunt.lastOperateStatus != 0) {
          _state = FAILURE;
        } else {
          _state = DONE;
        }
      }
      break;
    case DONE:
      _state = MEASURE;
      break;
    case FAILURE:
      Serial.print("ShuntCtl.measure - state reached FAILURE");
      break;
    default:
      break;
  }
}

int ShuntCtl::getState() { return _state; }

float* ShuntCtl::getResults() {
  float* results = new float[4];
  results[0] = _busVoltage_V;
  results[1] = _shuntVoltage_mV;
  results[2] = _current_mA;
  results[3] = _power_mW;
  return results;
}