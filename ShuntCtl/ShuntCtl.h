#ifndef SHUNTCTL_h
#define SHUNTCTL_h
#include <Arduino.h>
#include <Wire.h>

#include "DFRobot_INA219.h"

// Define a custom enum for INA219 I2C addresses
enum INA219_I2C_ADDRESS {
  INA219_ADDR_1 = INA219_I2C_ADDRESS1,
  INA219_ADDR_2 = INA219_I2C_ADDRESS2,
  INA219_ADDR_3 = INA219_I2C_ADDRESS3,
  INA219_ADDR_4 = INA219_I2C_ADDRESS4,
};

class ShuntCtl {
 public:
  using ErrorHandler = void (*)();
  ShuntCtl(INA219_I2C_ADDRESS address, ErrorHandler errorHandler);
  void init();
  void measure();
  int getState();
  float* getResults();

 private:
  const INA219_I2C_ADDRESS _address;
  DFRobot_INA219_IIC _shunt;

  // state stuff
  enum State { INIT, MEASURE, DONE, FAILURE };
  State _state;

  // some timing stuff we need
  unsigned long _currentRuntime = 0;
  unsigned long _lastRuntime = 0;
  const unsigned int _interval = 10000;

  // store readings
  float _busVoltage_V;
  float _shuntVoltage_mV;
  float _current_mA;
  float _power_mW;

  // errorHandler
  ErrorHandler _errorHandler;
};

#endif