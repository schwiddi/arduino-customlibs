#include "QueueCtl.h"

QueueCtl::QueueCtl(MqttClient &client, const char *broker, unsigned int port,
                   const char *aliveTopic, unsigned int interval,
                   const char *waterLevelTopic, const char *shuntBatTopic, const char *shuntSolarTopic)
    : _broker(broker),
      _port(port),
      _aliveTopic(aliveTopic),
      _interval(interval),
      _waterLevelTopic(waterLevelTopic),
      _client(client),
      _shuntBatTopic(shuntBatTopic),
      _shuntSolarTopic(shuntSolarTopic) {}

void QueueCtl::connect(void (*errorHandler)()) {
  // set "will" if we get disconnected
  _client.beginWill(_aliveTopic, _retainTrue, _qos);
  _client.print(_willPayload);
  _client.endWill();

  // then we connect to queue
  Serial.println("QueueCtl.connect - connecting to Queue");

  if (!_client.connect(_broker, _port)) {
    Serial.print("QueueCtl.connect - connection failed! Error code = ");
    Serial.println(_client.connectError());
    errorHandler();
  }

  Serial.print("QueueCtl.connect - connected with: ");
  Serial.print(_broker);
  Serial.print(":");
  Serial.println(_port);

  // set callback func on message receuve
  // _client.onMessage();
}

bool QueueCtl::status() { return _client.connected(); }

void QueueCtl::sendAlive() {
  if ((millis() - _lastKeepAliveTimer) > _interval) {
    _client.beginMessage(_aliveTopic, _retainFalse, _qos);
    _client.print("{ \"up\": 1, \"uptime\": ");
    _client.print(millis() / 1000);
    _client.print(" }");
    _client.endMessage();
    _lastKeepAliveTimer = millis();
  }
}

void QueueCtl::disconnect() {
  _client.beginMessage(_aliveTopic, _retainTrue, _qos);
  _client.print("{ \"up\": 0, \"uptime\": ");
  _client.print(millis() / 1000);
  _client.print(", \"cleanExit\": 1 }");
  _client.endMessage();
  _client.stop();
}

void QueueCtl::sendWLMeasures(int measures[6]) {
  Serial.println("QueueCtl.sendWLMeasures - sending measures to queue");
  // the given measures array has following values
  // 0. rawMeasure
  // 1. normalizedMeasure
  // 2. sumLiter
  // 3. literMin
  // 4. literMax
  // 5. fillPercentage

  // first we do the single value send to queue part
  const char *subTopics[] = {"/analog",   "/normalized", "/liter",
                             "/literMin", "/literMax",   "/percent"};

  for (int i = 0; i < 6; i++) {
    char tmpTopic[50];
    strcpy(tmpTopic, _waterLevelTopic);
    strcat(tmpTopic, subTopics[i]);
    _client.beginMessage(tmpTopic, _retainFalse, _qos);
    _client.print(measures[i]);
    _client.endMessage();
  }

  // here we send a json with the same values
  // so we can work on the receiving end with what we want

  // we build here the json topic.. man is c++ complicated
  char jsonTopic[50];
  strcpy(jsonTopic, _waterLevelTopic);
  strcat(jsonTopic, "/json");

  // and send a json string
  _client.beginMessage(jsonTopic, _retainFalse, _qos);
  _client.print("{ \"analog\": ");
  _client.print(measures[0]);
  _client.print(", \"normalized\": ");
  _client.print(measures[1]);
  _client.print(", \"liter\": ");
  _client.print(measures[2]);
  _client.print(", \"literMin\": ");
  _client.print(measures[3]);
  _client.print(", \"literMax\": ");
  _client.print(measures[4]);
  _client.print(", \"percent\": ");
  _client.print(measures[5]);
  _client.print(" }");
  _client.endMessage();
}

void QueueCtl::sendBatMeasures(float measures[4]) {
  Serial.println("QueueCtl.sendBatMeasures - sending measures to queue");
  // the given measures array has following values
  // 0. _busVoltage_V
  // 1. _shuntVoltage_mV
  // 2. _current_mA
  // 3. _power_mW

  // first we do the single value send to queue part
  const char *subTopics[] = {"/busvoltagev", "/shuntvoltagemv", "/currentma",
                             "/powermw"};

  for (int i = 0; i < 4; i++) {
    char tmpTopic[50];
    strcpy(tmpTopic, _shuntBatTopic);
    strcat(tmpTopic, subTopics[i]);
    _client.beginMessage(tmpTopic, _retainFalse, _qos);
    _client.print(measures[i]);
    _client.endMessage();
  }

  // here we send a json with the same values
  // so we can work on the receiving end with what we want

  // we build here the json topic.. man is c++ complicated
  char jsonTopic[50];
  strcpy(jsonTopic, _shuntBatTopic);
  strcat(jsonTopic, "/json");

  // and send a json string
  _client.beginMessage(jsonTopic, _retainFalse, _qos);
  _client.print("{ \"busVoltage_V\": ");
  _client.print(measures[0]);
  _client.print(", \"shuntVoltage_mV\": ");
  _client.print(measures[1]);
  _client.print(", \"current_mA\": ");
  _client.print(measures[2]);
  _client.print(", \"power_mW\": ");
  _client.print(measures[3]);
  _client.print(" }");
  _client.endMessage();
}

void QueueCtl::sendSolarMeasures(float measures[4]) {
  Serial.println("QueueCtl.sendSolarMeasures - sending measures to queue");
  // the given measures array has following values
  // 0. _busVoltage_V
  // 1. _shuntVoltage_mV
  // 2. _current_mA
  // 3. _power_mW

  // first we do the single value send to queue part
  const char *subTopics[] = {"/busvoltagev", "/shuntvoltagemv", "/currentma",
                             "/powermw"};

  for (int i = 0; i < 4; i++) {
    char tmpTopic[50];
    strcpy(tmpTopic, _shuntSolarTopic);
    strcat(tmpTopic, subTopics[i]);
    _client.beginMessage(tmpTopic, _retainFalse, _qos);
    _client.print(measures[i]);
    _client.endMessage();
  }

  // here we send a json with the same values
  // so we can work on the receiving end with what we want

  // we build here the json topic.. man is c++ complicated
  char jsonTopic[50];
  strcpy(jsonTopic, _shuntSolarTopic);
  strcat(jsonTopic, "/json");

  // and send a json string
  _client.beginMessage(jsonTopic, _retainFalse, _qos);
  _client.print("{ \"busVoltage_V\": ");
  _client.print(measures[0]);
  _client.print(", \"shuntVoltage_mV\": ");
  _client.print(measures[1]);
  _client.print(", \"current_mA\": ");
  _client.print(measures[2]);
  _client.print(", \"power_mW\": ");
  _client.print(measures[3]);
  _client.print(" }");
  _client.endMessage();
}