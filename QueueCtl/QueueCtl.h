#ifndef QUEUECTL_h
#define QUEUECTL_h

#include <Arduino.h>
#include <ArduinoMqttClient.h>

class QueueCtl {
 public:
  QueueCtl(MqttClient &client, const char *broker, unsigned int port,
           const char *aliveTopic, unsigned int interval,
           const char *waterLevelTopic, const char *shuntBatTopic, const char *shutSolarTopic);
  void connect(void (*errorHandler)());
  bool status();
  void sendAlive();
  // void reconnect() { connect(); };
  void disconnect();
  void sendWLMeasures(int wlMeasures[4]);
  void sendBatMeasures(float batMeasures[4]);
  void sendSolarMeasures(float solarMeasures[4]);

  // Place to store mqttClient
  MqttClient &_client;

 private:
  // broker stuff
  const char *_broker;
  const unsigned int _port;

  // general stuff
  const char *_aliveTopic;
  unsigned long _lastKeepAliveTimer = 0;
  const unsigned int _interval;
  const int _qos = 1;
  const bool _retainTrue = true;
  const bool _retainFalse = false;

  // stuff regarding will (disconnect)
  const char _willPayload[53] =
      "{ \"up\": 0, \"uptime\": 0, \"cleanExit\": 0 }";

  // topics
  const char *_waterLevelTopic;
  const char *_shuntBatTopic;
  const char *_shuntSolarTopic;
};

#endif