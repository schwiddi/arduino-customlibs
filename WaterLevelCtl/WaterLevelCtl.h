#ifndef WATERLEVELCTL_h
#define WATERLEVELCTL_h

#include <Arduino.h>

#include <vector>

class WaterLevelCtl {
 public:
  WaterLevelCtl(int sensorPin, unsigned int sensorMin, unsigned int sensorMax,
                unsigned int literMin, unsigned int literMax,
                unsigned int interval, unsigned int measureMax,
                unsigned int flatenOffset);
  void init();
  void measure();
  int getState();
  int* getResults();

 private:
  //  constants
  const int _sensorPin;
  const unsigned int _sensorMin;
  const unsigned int _sensorMax;
  const unsigned int _literMin;
  const unsigned int _literMax;
  const unsigned int _interval;
  const unsigned int _measureMax;
  const unsigned int _flatenOffset;

  // state stuff
  enum State { INIT, MEASURE, CALC, DONE };
  State _state;

  // measurements arr and a counter
  std::vector<unsigned int> _measures;
  int _measureCount;
  unsigned long _lastMeasureTime;

  // function to sort measures
  void sortMeasures();

  // some stuff for calc
  int _sumRaw;
  int _divider;

  // place to hold results
  int _results[6];
  int _rawMeasure;
  int _normalizedMeasure;
  int _sumLiter;
  int _fillPercentage;
};

#endif