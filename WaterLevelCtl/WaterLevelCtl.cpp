#include "WaterLevelCtl.h"

WaterLevelCtl::WaterLevelCtl(int sensorPin, unsigned int sensorMin,
                             unsigned int sensorMax, unsigned int literMin,
                             unsigned int literMax, unsigned int interval,
                             unsigned int measureMax, unsigned int flatenOffset)
    : _sensorPin(sensorPin),
      _sensorMin(sensorMin),
      _sensorMax(sensorMax),
      _literMin(literMin),
      _literMax(literMax),
      _interval(interval),
      _measureMax(measureMax),
      _flatenOffset(flatenOffset) {
  _state = INIT;
  _measures.resize(measureMax);
  _measureCount = 0;
  _lastMeasureTime = 0;
  _divider = measureMax - (flatenOffset * 2);
  // someday we need to come back and change this
  // logic should be that if we receive wrong values for max and flaten, we
  // lower the ofset ourself untill we are happy with the result
}

void WaterLevelCtl::init() {
  pinMode(_sensorPin, INPUT);
  _state = INIT;
  _measures.clear();
  _measureCount = 0;
  _lastMeasureTime = 0;
}

void WaterLevelCtl::measure() {
  switch (_state) {
    case INIT:
      _measures.clear();
      _measureCount = 0;
      _lastMeasureTime = millis();
      _state = MEASURE;
      break;
    case MEASURE:
      if (_measureCount < _measureMax) {
        if ((millis() - _lastMeasureTime) > _interval) {
          Serial.print("WaterLevelCtl.measure - doing measure ");
          Serial.print(_measureCount + 1);
          Serial.print("/");
          Serial.println(_measureMax);
          _measures.push_back(analogRead(_sensorPin));
          _measureCount++;
          _lastMeasureTime = millis();
        }
      } else {
        // if we have reached _measureMax we move state to calc
        _state = CALC;
      }
      break;
    case CALC:
      // first we sort the measures array
      sortMeasures();

      // then we calc a sum from a subset of the measures
      // that are in the middle of the array
      // and we get that by taking flatenOffset left and right
      for (int i = _flatenOffset; i < _measureMax - _flatenOffset; i++) {
        if (i == _flatenOffset) {
          // incase we are on the first value
          // we set the sum
          _sumRaw = _measures[i];
        } else {
          // else we add to it
          _sumRaw += _measures[i];
        }
      }

      // divide sum to get avg
      // round and cast to int for later use
      _rawMeasure = static_cast<int>(_sumRaw / static_cast<float>(_divider));

      // if the avg measured value is below or above sensor range
      // we normalize it
      // happens for example when the liquid freezes with the sensor in it :D
      if (_rawMeasure < _sensorMin) {
        _normalizedMeasure = _sensorMin;
      } else if (_rawMeasure > _sensorMax) {
        _normalizedMeasure = _sensorMax;
      } else {
        _normalizedMeasure = _rawMeasure;
      }

      // then we map the result into a liters and percent
      _sumLiter =
          map(_normalizedMeasure, _sensorMin, _sensorMax, _literMin, _literMax);
      _fillPercentage = map(_sumLiter, _literMin, _literMax, 0, 100);

      // and set the results
      // from where they can be read
      _results[0] = _rawMeasure;
      _results[1] = _normalizedMeasure;
      _results[2] = _sumLiter;
      _results[3] = _literMin;
      _results[4] = _literMax;
      _results[5] = _fillPercentage;

      _state = DONE;
      break;
    case DONE:
      _state = INIT;
      break;
    default:
      break;
  }
}

void WaterLevelCtl::sortMeasures() {
  for (int i = 0; i < _measureMax - 1; i++) {
    for (int j = 0; j < _measureMax - i - 1; j++) {
      if (_measures[j] > _measures[j + 1]) {
        // Swap the elements if they are in the wrong order
        int depth = _measures[j];
        _measures[j] = _measures[j + 1];
        _measures[j + 1] = depth;
      }
    }
  }
}

int WaterLevelCtl::getState() { return _state; }

int* WaterLevelCtl::getResults() { return _results; }