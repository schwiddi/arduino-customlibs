#ifndef PUMPCTL_h
#define PUMPCTL_h

#include <Arduino.h>

#include "RelaisCtl.h"

class PumpCtl {
 public:
  PumpCtl(RelaisCtl& pumpRelais, const char* direction,
          const float literPerMinute);
  void process();
  void setRuntime(unsigned long runtime);
  void setRuntimeLiter(unsigned int liter);

 private:
  // the pump relais we will control is stored here
  RelaisCtl& _pumpRelais;

  // the direction of the pump - sofar only used for output
  const char* _direction;

  // liter per minute of the pump measured at some point on seting up the
  // installation not used for now maybe i will do later handling stuff like,
  // pump 100 pls and then calc for how long the pump has to be ran
  const float _literPerMinute;

  // some timing stuff we need
  unsigned long _runtime = 0;
  unsigned long _currentRuntime = 0;
  unsigned long _lastRuntime = 0;
  unsigned long _loopTime = 0;

  // state stuff
  enum State { INIT, CHECK, PUMP, DONE };
  State _state = INIT;

  // some safety stuff
  unsigned int _runtimeMin = 2000;    // should be around one liter
  unsigned int _runtimeMax = 110000;  // should be 50 liters
};

#endif