#include "PumpCtl.h"

PumpCtl::PumpCtl(RelaisCtl& pumpRelais, const char* direction,
                 const float literPerMinute)
    : _pumpRelais(pumpRelais),
      _direction(direction),
      _literPerMinute(literPerMinute) {}

// 50 L = 1m 50s or 110s

void PumpCtl::process() {
  switch (_state) {
    case INIT:
      _lastRuntime = millis();
      _state = CHECK;
      break;
    case CHECK:
      if (_runtime > 0) {
        Serial.print("PumpCtl.process - ");
        Serial.print(_direction);
        Serial.print(" - runtime was set to (ms): ");
        Serial.println(_runtime);
        _state = PUMP;
      } else {
        _state = DONE;
      }
      _lastRuntime = millis();
      break;
    case PUMP:
      _currentRuntime = millis();
      _loopTime = _currentRuntime - _lastRuntime;
      _lastRuntime = _currentRuntime;

      if (_runtime >= _loopTime) {
        if (!_pumpRelais.getState()) {
          Serial.print("PumpCtl.process - ");
          Serial.print(_direction);
          Serial.println(" - Pump started");
          _pumpRelais.on();
        } else {
          // this takes care that we do the first loop of pumping no reducing of
          // pump time we only land here when the first full loop of pumping
          // happened cause we check if pump relais in on
          _runtime -= _loopTime;
        }
      } else {
        if (_pumpRelais.getState()) {
          Serial.print("PumpCtl.process - ");
          Serial.print(_direction);
          Serial.println(" - Pump stopped");
          _pumpRelais.off();
        }
        _runtime = 0;
        _state = DONE;
      }
      break;
    case DONE:
      _lastRuntime = millis();
      _state = CHECK;
      break;
    default:
      break;
  }
}

void PumpCtl::setRuntime(unsigned long runtime) {
  if (runtime == 0) {
    _runtime = 0;
  } else if (runtime > _runtimeMax) {
    _runtime = _runtimeMax;
  } else if (runtime < _runtimeMin) {
    _runtime = 0;
  } else {
    _runtime = runtime;
  }
}

void PumpCtl::setRuntimeLiter(unsigned int liter) {
  // we get liters and we know how mutch we do per second
  // so we calc a runtime and call setruntime with it

  // 100 liter - pump does 27.27 l per minute
  unsigned long runtime =
      (unsigned long)((float)liter * 60.0 * 1000.0 / _literPerMinute);

  setRuntime(runtime);
}